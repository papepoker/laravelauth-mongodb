<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Users;
use Auth;
use Validator;
use DB;
use Crypt;
use Input;
use Mail;

use Illuminate\Http\Request;

class AuthController extends Controller {

    public function login() {

    	$email = Input::get('email');
        $password = Input::get('password');
        $user = Users::where('email', '=', $email)->where('password', '=', $password)->get();

        if (!empty(Input::get('remember'))) {
            $remember = true;
        } else {
            $remember = false;
        }       
        
        if(!empty($user)) {
        	$data = Users::find($user->toArray()[0]['_id']);
        	Auth::login($data, $remember);
        	
        	$login_data = Auth::user();
        	if (Auth::check()) {
        		return response($login_data, 200);
        	}
        } else {
        	return response('email or password wrong !', 400);
        }

    }    

    public function checklogin() { 
    	return 'you already login';
    } 

    public function myaccount() { 
    	$login_data = Auth::user();
    	return response($login_data, 200);
    }

    public function logout() { 
    	Auth::logout();
    	return response('logout complete.', 200);
    }                 

}